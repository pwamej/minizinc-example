Tests passed: 100%
Performance: 0% - no solution at all, timeout
Competition: N/A

# Port Scheduling

1. Fork this project
2. Add @mateusz.slazynski as the new project's member (role: ``master``)
3. Change project permissions from ``internal`` to ``private``
4. Check the ``handout.pdf`` for the instructions. Ignore all sections about submitting to coursera.
5. Make sure that output contains a line of form ``obj = <objective>;`` where ``objective`` is the value of optimized function (as in the ``solve minimize <objective>``). 
6. Solve the problem!
7. Automated tests will be run periodically to check quality of your model. The results will be available on top of the ``README.md``. Details will be available in the ``test.log`` file. Of course you can run the tests by yourself - just run the ``test.sh`` script (with decent versions of ``bash`` and ``MiniZinc``). More tests may be added in the future.
8. If done before the deadline, contact @mateusz.slazynski via Slack, so he can check it earlier.
